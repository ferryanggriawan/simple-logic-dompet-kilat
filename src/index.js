function isPalindrome(text = "abcba") {
  const textArr = text.split("")
  const reverseTextArr = textArr.reverse()
  const reverseText = reverseTextArr.join("")

  if (text == reverseText) {
    console.log("Palindrome: ", text, "is palindrome")
  } else {
    console.log("Palindrome: ", text, "is not palindrome")
  }
}

function findPrimeByRange(start = 11, end = 40) {
  const prime = []

  for (let index = start; index <= end; index++) {
    let flag = 0

    for (let check = 2; check < index; check++) {
      if (index % check == 0) {
        flag = 1
      }
    }

    if (index > 1 && flag == 0) {
      prime.push(index)
    }
  }

  console.log("Prime number: ", prime)
}

function group(arr = []) {
  const groupArr = []
  let arrTemp = []
  arr.reduce((prev, current, currentIndex) => {
    if (prev != current) {
      arrTemp.push(prev)
      groupArr.push(arrTemp)
      prev = current
      arrTemp = []
    } else {
      arrTemp.push(prev)
    }

    if (currentIndex === arr.length - 1) {
      arrTemp.push(prev)
      groupArr.push(arrTemp)
      prev = current
      arrTemp = []
    }

    return prev
  })

  return groupArr
}

function countGroup(arr) {
  const groupArr = group(arr)
  const countArr = []
  groupArr.map((item) => {
    countArr.push([item.length, item[0]])
  })

  console.log("Count Same Group: ", countArr)
}

isPalindrome("akuaka")
findPrimeByRange(11, 40)
const arr = [
  "a",
  "a",
  "a",
  "b",
  "c",
  "c",
  "b",
  "b",
  "b",
  "d",
  "d",
  "e",
  "e",
  "e",
]
const groupArr = group(arr)
console.log("Group Array: ", groupArr)
countGroup(arr)
